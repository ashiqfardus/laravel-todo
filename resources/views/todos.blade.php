@extends('layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <form action="/create/todo" method="post">
                    {{csrf_field()}}
                    <input type="text" class="form-control input-sm" name="todo" placeholder="Create New Todo">

                </form>
            </div>
        </div>
        <hr>
        @foreach($todos as $todo)
            {{$todo->todo}}
            <a href="{{route('todo.delete',['id'=>$todo->id])}}" class="btn btn-danger">X</a>
            <a href="{{route('todo.update',['id'=>$todo->id])}}" class="btn btn-info btn-xs">Update</a>

        @if(!$todo->completed)
                <a href="{{route('todos.completed',['id'=>$todo->id])}}" class="btn btn-success btn-xs">Completed</a>
            @else
                <span class="text-success">Completed</span>
            @endif
            <hr>
        @endforeach
    </div>
@stop