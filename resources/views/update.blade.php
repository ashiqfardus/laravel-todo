@extends('layout')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <form action="{{route('todo.save',['id'=>$todo->id])}}" method="post">
                    {{csrf_field()}}
                    <input type="text" class="form-control input-sm" name="todo" value="{{$todo->todo}}">

                </form>
            </div>
        </div>

    </div>
@stop